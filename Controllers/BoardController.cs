﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Web;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using dotNetMVCFutabaChan.Models;

namespace dotNetMVCFutabaChan.Controllers
{
    public class BoardController : Controller
    {

        public Guid ThreadGuid;

        private readonly IHostingEnvironment _hostingEnvironment;

        public BoardController(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        /* 1. Create a new thread based on model. */
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult Index(MessageModel form, IFormFile imageFile)
        {
            // New guid for our thread.
            ThreadGuid = Guid.NewGuid();

            // New thread.
            ThreadModel thread = new ThreadModel();
            List<ThreadModel> threadList = new List<ThreadModel>();
            XmlSerializer xs = new XmlSerializer(typeof(List<ThreadModel>));
            Int64 maxThreadId = 0;

            // The index message for new thread.
            MessageModel msg = new MessageModel();
            List<MessageModel> msgList = new List<MessageModel>();
            XmlSerializer ms = new XmlSerializer(typeof(List<MessageModel>));
            double maxMsgId = 0;

            // Retrieve the largest index number for a thread which currently exists.
            if (System.IO.File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/ThreadList.xml")))
            {
                // Load existing XML file as a List<threadmodel>.
                try
                {
                    using (TextReader tr = new StreamReader(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/ThreadList.xml")))
                    {
                        threadList = (List<ThreadModel>)xs.Deserialize(tr);
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }

                // Find largest thread id. Done to maintain some level of superficial compatibility with existing chans.
                maxThreadId = threadList.Max(i => i.threadId) + 1;
            }

            // Create a thread.
            thread.threadId = maxThreadId;
            thread.threadGuid = ThreadGuid;
            thread.creationDate = DateTime.UtcNow;
            threadList.Add(thread);

            /*
             * Save it as XML file.
             * TODO: Check for directory, if it doesn't exist yet, create a directory 'Threads'.
             */
            try
            {
                if (!Directory.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads")))
                {
                    Directory.CreateDirectory(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads"));
                }

                if (!Directory.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Logs")))
                {
                    Directory.CreateDirectory(Path.Combine(_hostingEnvironment.ContentRootPath, "Logs"));
                }

                using (TextWriter tw = new StreamWriter(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/ThreadList.xml")))
                {
                    xs.Serialize(tw, threadList);
                }

                Log("New thread was created.");
                ViewData["SuccessMessage"] = "Thread Created!";
            }
            catch (System.Exception ex)
            {
                ViewData["FailMessage"] = "Shit Happened!";
                throw ex;
            }

            // Create a directory for our new thread.
            if (!System.IO.Directory.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString())))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString()));
                }
                catch (System.Exception ex)
                {

                    throw ex;
                }
            }

            /*
                We know for certain that this is a message with zero id in
                this particular thread so skip the check and just process the form.
            */
            msg.threadGuid = ThreadGuid;
            msg.msgId = maxMsgId;
            msg.name = form.name;
            msg.email = form.email;
            msg.subject = form.subject;
            // TODO: If no message is found, throw an error and get the fuck out of here.
            msg.msgContent = form.msgContent;
            msg.postDate = DateTime.UtcNow;
            msgList.Add(msg);

            // I probably want to validate the model.
            // if (ModelState.IsValid) {}

            // Try processing file if it exists.
            if (imageFile != null && imageFile.Length > 0)
            {
                var parsedContentDisposition = ContentDispositionHeaderValue.Parse(imageFile.ContentDisposition);
                string FilePath = parsedContentDisposition.FileName.Trim('"');
                string FileExtension = Path.GetExtension(FilePath);
                var fileRootPath = Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString() + "/");
                var fileNamePath = Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString() + "/" + msg.msgId.ToString() + FileExtension);
                var thumbnailNamePath = Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString() + "/thumbnails/" + msg.msgId.ToString() + FileExtension);
                var fileUrl = Path.Combine("images/threads/" + ThreadGuid.ToString() + "/" + msg.msgId.ToString() + FileExtension);
                
                // Sandcastle which was never built.
                // msg.imgFile = fileUrl;
                msg.imgFile = msg.msgId.ToString() + FileExtension;

                if (!System.IO.Directory.Exists(fileRootPath))
                {
                    try
                    {
                        System.IO.Directory.CreateDirectory(Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString()));
                    }
                    catch (System.Exception ex)
                    {
                        throw ex;
                    }
                }

                using (var fs = new FileStream(fileNamePath, FileMode.Create))
                {
                    imageFile.CopyTo(fs);
                }

                // Check that we have a thumbnails directory where to write.
                if (!System.IO.Directory.Exists(Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString() + "/thumbnails")))
                {
                    try
                    {
                        System.IO.Directory.CreateDirectory(Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString() + "/thumbnails"));
                    }
                    catch (System.Exception ex)
                    {
                        throw ex;
                    }
                }

                // Ok, lets figure a way to create a thumbnail next.
                try
                {

                    using (Image<Rgba32> image = Image.Load(fileNamePath))
                    {
                        image.Mutate(x => x
                        .Resize(0, 300));
                        image.Save(thumbnailNamePath); // Automatic encoder selected based on extension.
                    }

                }
                catch (System.Exception i)
                {
                    throw i;
                }

            }

            // Save message as XML file.
            try
            {
                using (TextWriter tw = new StreamWriter(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString() + "/" + ThreadGuid.ToString() + ".xml")))
                {
                    ms.Serialize(tw, msgList);
                }

                Log("New message was added to thread xxx.");
                ViewData["SuccessMessage"] = "Message Submitted!";
            }
            catch (System.Exception ex)
            {
                ViewData["FailMessage"] = "Shit Happened!";
                throw ex;
            }

            return this.RedirectToAction("Index", "Thread", new { guid = thread.threadGuid });
        }

        /* 2. List existing threads. */
        [HttpGet]
        public IActionResult Index(int? pageNum)
        {
            /* Ideal case:
                           if no number is specified, return shit from index number 0 up to 10 threads.
                           get number 1, return shit from index number 0 up to 10 threads.
                           get number 2, return shit from index number 9 up to 10 threads.
            */
            int page;
            int? threadNum = null;
            int threadsPerPage = 0;
            // ThreadModel thread = new ThreadModel();
            // MessageModel msg = new MessageModel();
            List<MessageModel> msgList = new List<MessageModel>();
            List<ThreadModel> threadList = new List<ThreadModel>();
            List<ShowThreadsModel> showThreadList = new List<ShowThreadsModel>();
            XmlSerializer xs = new XmlSerializer(typeof(List<ThreadModel>));
            XmlSerializer ms = new XmlSerializer(typeof(List<MessageModel>));

            if (pageNum == null)
            {
                page = 0;
            }
            else
            {
                page = (int)pageNum;
            }

            if (threadNum == null)
            {
                threadsPerPage = 9;
            }
            else
            {
                threadsPerPage = (int)threadNum;
            }

            if (System.IO.File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/ThreadList.xml")))
            {
                // Load existing XML file as a List<threadmodel>.
                try
                {
                    using (TextReader tr = new StreamReader(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/ThreadList.xml")))
                    {
                        threadList = (List<ThreadModel>)xs.Deserialize(tr);
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }

            /*
            * Get first three messages of a thread if they exists. And do a punch of other stuff.
            */
            for (var iter = 0; iter < threadList.Count; iter++)
            {

                /* This should be a separate function somewhere to be honest. */
                if (System.IO.File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + threadList[iter].threadGuid.ToString() + "/" + threadList[iter].threadGuid.ToString() + ".xml")))
                {
                    /* Load existing XML file as a List<messagemodel>. */
                    try
                    {
                        using (TextReader tr = new StreamReader(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + threadList[iter].threadGuid.ToString() + "/" + threadList[iter].threadGuid.ToString() + ".xml")))
                        {
                            msgList = (List<MessageModel>)ms.Deserialize(tr);
                        }
                    }
                    catch (System.Exception ex)
                    {
                        throw ex;
                    }
                }

                ShowThreadsModel Stm = new ShowThreadsModel();
                Stm.threadGuid = threadList[iter].threadGuid;
                Stm.lastPostDateTime = msgList.Last().postDate;
                Stm.numberOfMessages = msgList.Count;
                /* TODO: Choose the first message + last 6 messages and push them into the list. */
                Stm.messages = (List<MessageModel>)msgList.Take(6).ToList();
                showThreadList.Add(Stm);
            }

            /* Sort threads by post date and time. */
            showThreadList.Sort((x, y) => -1 * DateTime.Compare(x.lastPostDateTime, y.lastPostDateTime));

            /*
             * TODO: Validity check... or something... if specified range exceeds the existing number of threads
             * then change that range value to whatever shit is left in that collection.
             */
            var tmpPage = showThreadList.Skip(Math.Max(0, showThreadList.Count() - threadsPerPage));

            /* Get total pages when threads per page is x. */
            float total = showThreadList.Count() / threadsPerPage;
            int maxPages = (int)Math.Round(total, MidpointRounding.AwayFromZero);

            /*
             * Get a range of threads per page.
             * TODO: Skip if no threads in XML files are listed.
             */
            if (threadsPerPage >= 10)
            {
                ViewData["ShowThreadsList"] = showThreadList.GetRange(page, threadsPerPage);
            }
            else
            {
                ViewData["ShowThreadsList"] = showThreadList;
            }

            ViewData["ThreadList"] = threadList;

            /* currpage = shit */
            ViewBag.CurrentPage = tmpPage;
            ViewBag.MaxPages = (int)maxPages;

            return View();
        }

        /* 2. List existing threads as catalog. */
        [HttpGet]
        public IActionResult Catalog()
        {
            ThreadModel thread = new ThreadModel();
            List<ThreadModel> threadList = new List<ThreadModel>();
            XmlSerializer xs = new XmlSerializer(typeof(List<ThreadModel>));

            if (System.IO.File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/ThreadList.xml")))
            {
                // Load existing XML file as a List<threadmodel>.
                try
                {
                    using (TextReader tr = new StreamReader(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/ThreadList.xml")))
                    {
                        threadList = (List<ThreadModel>)xs.Deserialize(tr);
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }

            ViewData["ThreadList"] = threadList;

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public void Log(string logMessage)
        {
            try
            {
                var postDate = DateTime.Now.ToString("yyyy-dd-M");
                var webRoot = _hostingEnvironment.ContentRootPath;
                var logPath = Path.Combine(webRoot, "Logs/" + postDate + ".log");

                using (StreamWriter sw = System.IO.File.AppendText(logPath))
                {
                    sw.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " - " + logMessage);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
