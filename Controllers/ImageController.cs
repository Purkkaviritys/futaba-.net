using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using dotNetMVCFutabaChan.Models;

namespace dotNetMVCFutabaChan.Controllers
{
    public class ImageController : Controller
    {

        private readonly IHostingEnvironment _hostingEnvironment;

        public ImageController(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult Index(PostModel post, IFormFile imageFile)
        {
            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images");
            Guid guid = post.guid;
            DateTime postDate = post.postDate;
            string name = post.name;
            string email = post.email;
            string subtitle = post.subtitle;
            string comment = post.comment;
            string url = post.url;
            string host = post.host;
            string password = post.password;
            string imageFileName = post.imageFileName;
            string imageFileExtension = post.imageFileName;
            int thumbWidth = post.thumbWidth;
            int thumbHeight = post.thumbHeight;
            DateTime postTime = post.postTime;
            bool textOnly = post.textOnly;

            return View();
        }
    }
}
