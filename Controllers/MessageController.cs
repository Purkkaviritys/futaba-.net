using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using dotNetMVCFutabaChan.Models;
using System.Net.Http.Headers;

namespace dotNetMVCFutabaChan.Controllers
{
    public class MessageController : Controller
    {

        private readonly IHostingEnvironment _hostingEnvironment;

        public MessageController(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        public IActionResult Index()
        {

            MessageModel msg = new MessageModel();
            List<MessageModel> msgList = new List<MessageModel>();
            XmlSerializer xs = new XmlSerializer(typeof(List<MessageModel>));

            if (System.IO.File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/messages.xml")))
            {
                // Load existing XML file as a List<messagemodel>.
                try
                {
                    using (TextReader tr = new StreamReader(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/messages.xml")))
                    {
                        msgList = (List<MessageModel>)xs.Deserialize(tr);
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }

            ViewData["MessageList"] = msgList;
            ViewData["InfoMessage"] = "It's possible to use tags in your post.";

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult Index(MessageModel form, IFormFile imageFile)
        {
            MessageModel msg = new MessageModel();
            List<MessageModel> msgList = new List<MessageModel>();
            XmlSerializer xs = new XmlSerializer(typeof(List<MessageModel>));
            double maxMsgId = 0;

            if (System.IO.File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/messages.xml")))
            {
                // Load existing XML file as a List<messagemodel>.
                try
                {
                    using (TextReader tr = new StreamReader(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/messages.xml")))
                    {
                        msgList = (List<MessageModel>)xs.Deserialize(tr);
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                // Find largest msg id.
                maxMsgId = msgList.Max(i => i.msgId) + 1;
            }

            // Process form and write new information to List.
            msg.threadGuid = Guid.NewGuid();
            msg.msgId = maxMsgId;
            msg.msgContent = form.msgContent;
            msg.postDate = DateTime.UtcNow;
            msgList.Add(msg);

            // I probably want to validate the models automatically.
            // if (ModelState.IsValid) {}

            // Try processing file if it exists.
            if (imageFile != null && imageFile.Length > 0) {
                var parsedContentDisposition = ContentDispositionHeaderValue.Parse(imageFile.ContentDisposition);
                string FilePath = parsedContentDisposition.FileName.Trim('"');
                string FileExtension = Path.GetExtension(FilePath);
                var uploadDir = _hostingEnvironment.ContentRootPath + $@"wwwroot\images\{msg.msgId}\";
                
                if(!Directory.Exists(uploadDir))
                {
                    Directory.CreateDirectory(uploadDir);
                }
                var imageUrl = uploadDir + msg.msgId + FileExtension;
                imageFile.CopyTo(new FileStream(uploadDir, FileMode.Create));
            }

            // Save it as XML file.
            try
            {
                using (TextWriter tw = new StreamWriter(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/messages.xml")))
                {
                    xs.Serialize(tw, msgList);
                }

                Log("New message was added to thread xxx.");
                ViewData["SuccessMessage"] = "Message Submitted!";
            }
            catch (System.Exception ex)
            {
                ViewData["FailMessage"] = "Shit Happened!";
                throw ex;
            }
            
            return this.RedirectToAction("Message");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public void Log(string logMessage)
        {
            try
            {
                var postDate = DateTime.Now.ToString("yyyy-dd-M");
                var webRoot = _hostingEnvironment.ContentRootPath;
                var logPath = Path.Combine(webRoot, "Logs/" + postDate + ".log");

                using (StreamWriter sw = System.IO.File.AppendText(logPath))
                {
                    sw.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " - " + logMessage);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
