using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Web;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using dotNetMVCFutabaChan.Models;


namespace dotNetMVCFutabaChan.Controllers
{
    public class ThreadController : Controller
    {

        public Guid ThreadGuid;

        private readonly IHostingEnvironment _hostingEnvironment;

        public ThreadController(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        /* 2. Show a thread based on its index. */
        [HttpGet]
        public IActionResult Index(Guid guid)
        {
            ThreadGuid = guid;
            MessageModel msg = new MessageModel();
            List<MessageModel> msgList = new List<MessageModel>();
            XmlSerializer xs = new XmlSerializer(typeof(List<MessageModel>));

            /*
                0. Create a directory for the thread.
                1. Open up a xml file with the specified guid.
                2. Push its contents to view.                        
            */

            if (!System.IO.Directory.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString())))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString()));                    
                }
                catch (System.Exception ex)
                {
                    
                    throw ex;
                }
            }

            if (System.IO.File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString() + "/" + ThreadGuid.ToString() + ".xml")))
            {
                // Load existing XML file as a List<messagemodel>.
                try
                {
                    using (TextReader tr = new StreamReader(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString() + "/" + ThreadGuid.ToString() + ".xml")))
                    {
                        msgList = (List<MessageModel>)xs.Deserialize(tr);
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }

            ViewData["MessageList"] = msgList;
            ViewData["ThreadGuid"] = guid;
            ViewData["InfoMessage"] = "This is a thread: " + guid.ToString() + ".";

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult AddMessage(MessageModel form, IFormFile imageFile)
        {
            ThreadGuid = form.threadGuid;
            MessageModel msg = new MessageModel();
            List<MessageModel> msgList = new List<MessageModel>();
            XmlSerializer xs = new XmlSerializer(typeof(List<MessageModel>));
            double maxMsgId = 0;

            if (!System.IO.Directory.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString())))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString()));                    
                }
                catch (System.Exception ex)
                {
                    
                    throw ex;
                }
            }

            if (System.IO.File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString() + "/" + ThreadGuid.ToString() + ".xml")))
            {
                // Load existing XML file as a List<messagemodel>.
                try
                {
                    using (TextReader tr = new StreamReader(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString() + "/" + ThreadGuid.ToString() + ".xml")))
                    {
                        msgList = (List<MessageModel>)xs.Deserialize(tr);
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                // Find largest msg id.
                maxMsgId = msgList.Max(i => i.msgId) + 1;
            }

            // Process form and write new information to List.
            msg.threadGuid = ThreadGuid;
            msg.msgId = maxMsgId;
            msg.name = form.name;
            msg.email = form.email;
            msg.subject = form.subject;
            msg.msgContent = form.msgContent;
            msg.postDate = DateTime.UtcNow;
            msgList.Add(msg);

            // I probably want to validate the model.
            // if (ModelState.IsValid) {}

            // Try processing file if it exists.
            if (imageFile != null && imageFile.Length > 0) {
                var parsedContentDisposition = ContentDispositionHeaderValue.Parse(imageFile.ContentDisposition);
                string FilePath = parsedContentDisposition.FileName.Trim('"');
                string FileExtension = Path.GetExtension(FilePath);
                var fileRootPath = Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString() + "/");
                var fileNamePath = Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString() + "/" + msg.msgId.ToString() + FileExtension);
                var thumbnailNamePath = Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString() + "/thumbnails/" + msg.msgId.ToString() + FileExtension);
                var fileUrl = Path.Combine("images/threads/" + ThreadGuid.ToString() + "/" + msg.msgId.ToString() + FileExtension);
                // msg.imgFile = fileUrl;
                msg.imgFile = msg.msgId.ToString() + FileExtension;

                if (!System.IO.Directory.Exists(fileRootPath))
                {
                    try
                    {
                        System.IO.Directory.CreateDirectory(Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString()));                    
                    }
                    catch (System.Exception ex)
                    {
                        throw ex;
                    }
                }

                using (var fs = new FileStream(fileNamePath, FileMode.Create))
                {
                    imageFile.CopyTo(fs);
                }

                // Check that we have a thumbnails directory where to write.
                if (!System.IO.Directory.Exists(Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString() + "/thumbnails")))
                {
                    try
                    {
                        System.IO.Directory.CreateDirectory(Path.Combine(_hostingEnvironment.WebRootPath, "images/threads/" + ThreadGuid.ToString() + "/thumbnails"));
                    }
                    catch (System.Exception ex)
                    {
                        throw ex;
                    }
                }

                // Ok, lets figure a way to create a thumbnail next.
                try {

                    using (Image<Rgba32> image = Image.Load(fileNamePath))
                    {
                        image.Mutate(x => x
                        .Resize(0, 300));
                        // Automatic encoder selected based on extension.
                        image.Save(thumbnailNamePath);
                    }

                } catch (System.Exception i) {
                    throw i;
                }

            }

            // Save it as XML file.
            try
            {
                using (TextWriter tw = new StreamWriter(Path.Combine(_hostingEnvironment.ContentRootPath, "Threads/" + ThreadGuid.ToString() + "/" + ThreadGuid.ToString() + ".xml")))
                {
                    xs.Serialize(tw, msgList);
                }

                Log("New message was added to thread xxx.");
                ViewData["SuccessMessage"] = "Message Submitted!";
            }
            catch (System.Exception ex)
            {
                ViewData["FailMessage"] = "Shit Happened!";
                throw ex;
            }

            return this.RedirectToAction("Index", new { guid = ThreadGuid });
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public void Log(string logMessage)
        {
            try
            {
                var postDate = DateTime.Now.ToString("yyyy-dd-M");
                var webRoot = _hostingEnvironment.ContentRootPath;
                var logPath = Path.Combine(webRoot, "Logs/" + postDate + ".log");

                using (StreamWriter sw = System.IO.File.AppendText(logPath))
                {
                    sw.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " - " + logMessage);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

    }
}