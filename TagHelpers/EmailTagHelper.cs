using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace dotNetMVCFutabaChan.TagHelpers
{
    public class EmailTagHelper : TagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            // Replaces <email> with <a> tag
            output.TagName = "a";
        }
    }
}