using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dotNetMVCFutabaChan.Models
{
    public class ShowThreadsModel
    {
        public const Int32 initialImageSize = 256;
        public Guid threadGuid { get; set; }
        public Int32 numberOfMessages {get; set; }
        public DateTime lastPostDateTime { get; set; }
        public List<MessageModel> messages { get; set; }
    }
}