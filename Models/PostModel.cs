using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace dotNetMVCFutabaChan.Models
{
    public class PostModel
    {
        public Guid guid { get; set; }
        
        public DateTime postDate { get; set; }

        [Display(Name = "Name")]
        public string name { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Subtitle")]
        public string subtitle { get; set; }

        [Required]
        [Display(Name = "Comment")]
        public string comment { get; set; }
        
        public string url { get; set; }
        
        public string host { get; set; }

        [Display(Name = "Deletion Key")]
        public string password { get; set; }
        
        public IFormFile imageFile { get; set; }
        
        public string imageFileName { get; set; }
        
        public string imageFileExtension { get; set; }
        
        public int thumbWidth { get; set; }
        
        public int thumbHeight { get; set; }

        public DateTime postTime { get; set; }

        [Display(Name = "Text Only Thread")]
        public bool textOnly { get; set; }
    }
}