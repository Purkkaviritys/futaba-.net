/** TODO: This model will hold list of existing threads and
their creation dates and will be run once a day and old threads
are removed from list and from disk. */

/** TODO: Thread(threadguid, creationdate, lastpostdate) */

/** Each thread xml file is has guid as a name. */

using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace dotNetMVCFutabaChan.Models
{

    [Serializable]
    public class ThreadModel : ISerializable
    {
        [Required(ErrorMessage = "Thread count should be increased.")]
        public Int64 threadId { get; set; }

        [Required(ErrorMessage = "Thread needs its own Guid.")]
        public Guid threadGuid { get; set; }

        [Required(ErrorMessage = "Thread must have a creation date.")]
        public DateTime creationDate { get; set; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("threadId", threadId);
            info.AddValue("threadGuid", threadGuid);
            info.AddValue("creationDate", creationDate);
        }

        public ThreadModel() : base() { }

        public ThreadModel(SerializationInfo info, StreamingContext context)
        {
            threadId = (Int64)info.GetValue("threadId", typeof(Int64));
            threadGuid = (Guid)info.GetValue("threadGuid", typeof(Guid));
            creationDate = (DateTime)info.GetValue("postDate", typeof(DateTime));
        }
    }

    [XmlRoot("threads")]
    public class ThreadList
    {
        public ThreadList()
        {
            Threads = new List<ThreadModel>();
        }

        [XmlElement("thread")]
        public List<ThreadModel> Threads { get; set; }
    }
}