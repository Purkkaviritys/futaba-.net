using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace dotNetMVCFutabaChan.Models
{

    [Serializable]
    public class MessageModel : ISerializable
    {
        public Guid threadGuid { get; set; }

        public double msgId { get; set; }

        public DateTime postDate { get; set; }

        [Display(Name = "Name")]
        public string name { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Subject")]
        public string subject { get; set; }

        [Display(Name = "My Message")]
        [Required(ErrorMessage = "A message is required.")]
        [StringLength(5000)]
        public String msgContent { get; set; }

        // [Required(ErrorMessage ="Please Upload a Valid Image File")]
        // [DataType(DataType.Upload)]
        // [Display(Name ="Upload Image")]
        // [FileExtensions(Extensions ="jpg,png,gif,jpeg,bmp,svg")]
        // [XmlIgnore()]
        public string imgFile { get; set; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("threadGuid", threadGuid);
            info.AddValue("msgId", msgId);
            info.AddValue("postDate", postDate);
            info.AddValue("name", name);
            info.AddValue("email", email);
            info.AddValue("subject", subject);
            info.AddValue("msgContent", msgContent);
            info.AddValue("imgFile", imgFile);
        }

        public MessageModel() : base() { }

        public MessageModel(SerializationInfo info, StreamingContext context)
        {
            threadGuid = (Guid)info.GetValue("threadGuid", typeof(Guid));
            msgId = (double)info.GetValue("msgId", typeof(double));
            postDate = (DateTime)info.GetValue("postDate", typeof(DateTime));
            name = (String)info.GetValue("name", typeof(String));
            email = (String)info.GetValue("email", typeof(String));
            subject = (String)info.GetValue("subject", typeof(String));
            msgContent = (String)info.GetValue("msgContent", typeof(String));
            imgFile = (String)info.GetValue("imgFile", typeof(String));
        }
    }

    [XmlRoot("messages")]
    public class MessageList
    {
        public MessageList()
        {
            Messages = new List<MessageModel>();
        }

        [XmlElement("message")]
        public List<MessageModel> Messages { get; set; }
    }
}